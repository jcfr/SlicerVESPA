set(proj Boost)

# Set dependency list
set(${proj}_DEPENDS
    ""
)

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    message(FATAL_ERROR "Enabling ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED ${proj}_INCLUDE_DIR AND NOT EXISTS ${${proj}_INCLUDE_DIR})
    message(FATAL_ERROR "${proj}_INCLUDE_DIR [${${proj}_INCLUDE_DIR}] variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_INCLUDE_DIR AND NOT ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    if(WIN32)
        set(boost_url "https://boostorg.jfrog.io/artifactory/main/release/1.81.0/source/boost_1_81_0.zip")
        set(boost_hash "6e689b266b27d4db57f648b1e5c905c8acd6716b46716a12f6fc73fc80af842e")
    else()
        set(boost_url "https://boostorg.jfrog.io/artifactory/main/release/1.81.0/source/boost_1_81_0.tar.gz")
        set(boost_hash "205666dea9f6a7cfed87c7a6dfbeb52a2c1b9de55712c9c1a87735d7181452b6")
    endif()

    ExternalProject_Add(${proj}
        ${${proj}_EP_ARGS}
        
        URL ${boost_url}
        URL_HASH SHA256=${boost_hash}
        
        SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj}
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        DEPENDS
            ${${proj}_DEPENDS}
    )

    set(Boost_INCLUDE_DIR ${CMAKE_BINARY_DIR}/${proj})
else()
    ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(Boost_INCLUDE_DIR)

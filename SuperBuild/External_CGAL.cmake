set(proj CGAL)

# Set dependency list
# Even if CGAL uses boost, eigen, gmp and mpfr in its code, we don't build CGAL since it is a header only library
# The dependencies will be passed down to CGAL config module in our code
set(${proj}_DEPENDS
    ""
)

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj)

if(${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    message(FATAL_ERROR "Enabling ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED ${proj}_DIR AND NOT EXISTS ${${proj}_DIR})
    message(FATAL_ERROR "${proj}_DIR [${${proj}_DIR}] variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${SUPERBUILD_TOPLEVEL_PROJECT}_USE_SYSTEM_${proj})
    ExternalProject_Add(${proj}
        ${${proj}_EP_ARGS}
        
        GIT_REPOSITORY https://github.com/CGAL/cgal.git
        GIT_TAG v5.5.1

        SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj}
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        DEPENDS
            ${${proj}_DEPENDS}
    )

    set(CGAL_DIR ${CMAKE_BINARY_DIR}/${proj})
else()
    ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(CGAL_DIR)

# SlicerVESPA

SlicerVESPA is a Slicer extension that makes [VESPA](https://gitlab.kitware.com/vtk-cgal/vespa) algorithms available in Slicer.

# Algorithms
The following algorithms are available:
 - Alpha wrapping
 - Boolean Operation
 - Isotropic Remesher
 - Mesh Subdivision
 - Patch Filling
 - Shape Smoothing

# Building

SlicerVESPA can be built against any Slicer build, or built as an extension in a SlicerCAT

### Against a Slicer build:
```
$ cmake -S SlicerVESPA -B SlicerVESPA-build -DSlicer_DIR=Slicer-build
$ cmake --build SlicerVESPA-build --config Release
$ cmake --install SlicerVESPA-build --config Release --prefix SlicerVESPA-install
```

### In a SlicerCAT application
```
set(Slicer_EXTENSION_SOURCE_DIRS <path-to-SlicerVESPA>)
```

# Usage

Modules are in the VESPA folder in slicer module finder. They can also be called from python code like any other CLI module.

# License

SlicerVESPA is distributed under the terms of the VTK-CGAL license, see LICENSE.txt for details

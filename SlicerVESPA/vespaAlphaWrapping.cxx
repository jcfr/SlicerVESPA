#include "vespaAlphaWrappingCLP.h"

#include <vtkCGALAlphaWrapping.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

extern "C" 
{

int ModuleEntryPoint(int argc, char* argv[])
{
  PARSE_ARGS

  vtkNew<vtkXMLPolyDataReader> polyReader;
  polyReader->SetFileName(poly.c_str());

  vtkNew<vtkCGALAlphaWrapping> alphaWrapping;
  alphaWrapping->SetInputConnection(polyReader->GetOutputPort());
  alphaWrapping->SetAlpha(alpha);
  alphaWrapping->SetOffset(offset);
  alphaWrapping->SetAbsoluteThresholds(absoluteThresholds);
 
  vtkNew<vtkXMLPolyDataWriter> writer;
  writer->SetFileName(output.c_str());
  writer->SetInputConnection(alphaWrapping->GetOutputPort());
 
  return writer->Write() ? 0 : 1;
}

}